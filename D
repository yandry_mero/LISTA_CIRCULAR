package listayandrycircular;
class CNodo {
	int dato;
	CNodo siguiente;
        
	public CNodo()	{
            siguiente = null;
	}
}

class CLista {
    CNodo cabeza;
    public CLista()	{
            cabeza = null;
    }

    public void Insertar(int dat) {//metodo para ingresamos datos
        CNodo NuevoNodo;//se crearan nodo
        CNodo antes, luego;//los nodos creados son antes y luego 
        NuevoNodo = new CNodo();
        NuevoNodo.dato=dat;//El nuevo nodo se establecera al dato ingresado
        int ban=0;//se crea banddera 
        if (Vacia()){//preguntamos si la lista esta vacia 
            NuevoNodo.siguiente=NuevoNodo;//El nodo creado pasara hacer el nuevo nodo creado
            cabeza = NuevoNodo;//cabeza sera el nuevo nodo creado
        }
        else {  if (dat<cabeza.dato) {//se pregunta si el dato es menor a cabeza.dato
                    luego=cabeza;
                    antes=cabeza;
                        do{
                          antes=luego;
                          luego=luego.siguiente;
                        }while(luego!=cabeza);
                        antes.siguiente=NuevoNodo;
                        NuevoNodo.siguiente=cabeza;
                        cabeza = NuevoNodo;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                }
                else {  antes=cabeza;
                        luego=cabeza;
                        while (ban==0){//pregujnta si la bandera es igual igual a cero 
                            if (dat>=luego.dato) {//se preguntara si el dato es mayor e igual a luego.dato
                                antes=luego;
                                luego=luego.siguiente;
                            }
                            if (luego==cabeza){
                                ban=1;
                            }
                            else {
                                    if (dat<luego.dato){//se pregunta si el dato es menor a luego.dato
                                        ban=1;
                                    }
                            }
                        }
                        antes.siguiente=NuevoNodo;
                        NuevoNodo.siguiente=luego;
                }
        }
    }
    public void Eliminar(int dat) {//metodo para eliminar dato
        CNodo antes,luego; //Se crea dos Cndo ant y lue como controladores
        int ban=0;
        if (Vacia()) {   //se pregunta si esta vacia 
            System.out.print("Lista se encuentra vacía ");//mostraremos mensaje 
        }
        else {  if (dat<cabeza.dato) {//pregunta si dato es menor a cabeza.dato
                    System.out.print("dato no existe en la lista ");//nos mostrara mensaje 
                }
                else {
                        if (dat==cabeza.dato) {//pregunta si el dato es igual igual a cabeza.dato
                            luego=cabeza;
                            antes=cabeza;
                                while(ban==0){//pregunta si la bandera es igual igual a cero 
                                    if(luego.siguiente==cabeza){//pregunta si luego.siguiente es igual igual a cabeza
                                        ban=1;
                                    }
                                  antes=luego;
                                  luego=luego.siguiente;
                                }
                                    if(luego.siguiente==cabeza){
                                      cabeza=null;  
                                    }
                                    else{
                                        cabeza=cabeza.siguiente;
                                         antes.siguiente=cabeza;
                                    }
                        }
                        else {  antes=cabeza;
                                luego=cabeza;
                                while (ban==0) { //pregunta si la bandera es igual igul a cero
                                    if (dat>luego.dato) {//pregunta si el dato es mayor a luego.dato
                                        antes=luego;
                                        luego=luego.siguiente;
                                    }
                                    else ban=1;
                                    if (luego==cabeza) { //pregunta si lue es igual igual a cabeza
                                        ban=1;
                                    }
                                    else {
                                            if (luego.dato==dat) //pregunta si luego es igual igual a cabeza
                                                ban=1;
                                    }
                                }
                                if (luego==cabeza) { //pregunta si luego.dato es igual igual a dato
                                    System.out.print(" no existe dato en la Lista ");//muestra mensaje
                                }
                                else {
                                        if (dat==luego.dato) {//pregunta si el dato es igual igual a luego.dato
                                            antes.siguiente=luego.siguiente;
                                        }
                                        else 
                                            System.out.print(" no existe dato en la Lista ");
                                }
                        }
                }
        }
    }

    public boolean Vacia() {
        return(cabeza==null);
    }

    public void Visualizar() {//metodo visualizacion que nos permitira ver los datos 
        CNodo Temporal;  // se cre un Cndo llamado Temporal
        Temporal=cabeza; //Temporal lo iguala a cabeza
        if (!Vacia()) { //pregunta si la lista esta vacia
            do {
                System.out.print(" " + Temporal.dato +" ");//mensaje
                Temporal = Temporal.siguiente;
            }while(Temporal!=cabeza); //mientras Temporal sea diferente o igual a cabeza
            System.out.println(""); //mensaje
        }
        else
            System.out.println("Lista esta  vacía");//muestra mensaje
    }
}

public class ListaYandryCircular {
    public static void main(String args[]) {
        CLista objLista= new CLista();
        System.out.println("La lista queda con los siguientes datos");
        objLista.Insertar(100);//se inserta nuevo dato
        objLista.Insertar(5);
       
        objLista.Visualizar();
        System.out.println("Lista sin dato ");
        //eliminaremos datos 
        objLista.Eliminar(5);
        objLista.Visualizar();
    }
}